const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

//      1       //
function findMoviesWithEarningsMoreThan500M (movies) {
    const moviesWithEarningsMoreThan500 = Object.entries(movies).filter((movie) => {
        const movieDetails = movie[1];
        const earnings = +movieDetails.totalEarnings.replace(/[^0-9]/g, '');
        // console.log(earnings);ß
        return earnings > 500;
    })
    return Object.fromEntries(moviesWithEarningsMoreThan500);
}
console.log('movies with earnings more than $500 M:', findMoviesWithEarningsMoreThan500M(favouritesMovies));

//      2       //
function findMoviesWith3oscarsAndMoreThan500MEarnings (movies) {
    const moviesWith3oscarsAndMoreThan500MEarnings = Object.entries(movies).filter((movie) => {
        const movieDetails = movie[1];
        const earnings = +movieDetails.totalEarnings.replace(/[^0-9]/g, '');
        const oscars = movieDetails.oscarNominations;

        return earnings > 500 && oscars > 3;
    }) 
    return Object.fromEntries(moviesWith3oscarsAndMoreThan500MEarnings);
}
console.log('movies with earnings more than 500 M && more than 3 oscar nominations:', findMoviesWith3oscarsAndMoreThan500MEarnings(favouritesMovies));

//      3       //
function findAllMoviesOfLeo (movies) {
    const actorToSearch = 'leonardo dicaprio';
    const moviesOfLeo = Object.entries(movies).filter((movie) => {
        const movieDetails = movie[1];
        const actors = movieDetails.actors;
        const foundActor = actors.filter((name) => {
            if(name.trim().toLowerCase() === actorToSearch) {
                return true;
            } else {
                return false;
            }
        })
        if(foundActor[0]) {
            return true;
        } else {
            return false;
        }
    })
    return Object.fromEntries(moviesOfLeo);
}
console.log('all movies of Leonardo Decaprio:', findAllMoviesOfLeo(favouritesMovies));

//      4       //
function sortMoviesByRating (movies) {
    const sortedMovies = Object.entries(movies).sort((movie1, movie2) => {
        const movieDetails1 = movie1[1];
        const movieDetails2 = movie2[1];
        const earnings1 = +movieDetails1.totalEarnings.replace(/[^0-9]/g, '');
        const rating1 = movieDetails1.imdbRating;
        const earnings2 = +movieDetails2.totalEarnings.replace(/[^0-9]/g, '');
        const rating2 = movieDetails2.imdbRating;
        // console.log(rating1, rating2, earnings1, earnings2)
        if(rating1 < rating2) {
            return 1;
        } else if(rating1 > rating2) {
            return -1;
        } else {
            if(earnings1 < earnings2) {
                return 1;
            } else if(earnings1 > earnings2) {
                return -1;
            } else {
                return 0;
            }
        }
    })

    return Object.fromEntries(sortedMovies);
}
console.log('movies sorted by rating and earnings:', sortMoviesByRating(favouritesMovies));

//      5       //
function groupMoviesByGenre (movies) {
    // initializing an object to store the movies by genre in the given order
    const moviesByGenre = {'drama':[], 'sci-fi':[], 'adventure':[], 'thriller':[], 'crime':[]};
   
    const groupedMovies = Object.keys(movies).map((movieName) => {
        // console.log('movie name:', movieName);
        if(movies[movieName].genre.includes('drama')) {
            return ['drama', movieName, movies[movieName]];
        } else if(movies[movieName].genre.includes('sci-fi')) {
            return ['sci-fi', movieName, movies[movieName]];
        } else if(movies[movieName].genre.includes('adventure')) {
            return ['adventure', movieName, movies[movieName]];
        } else if(movies[movieName].genre.includes('thriller')) {
            return ['thriller', movieName, movies[movieName]];
        } else {
            return ['crime', movieName, movies[movieName]];
        }
    }).reduce((acc, value) => {
        const [movieGenre, movieName, movieInfo] = value;
        acc[movieGenre].push(Object.fromEntries([[movieName, movieInfo]]));
        return acc;
    }, moviesByGenre);
    
    return groupedMovies;
}
console.log('movies grouped by genres:', groupMoviesByGenre(favouritesMovies));
